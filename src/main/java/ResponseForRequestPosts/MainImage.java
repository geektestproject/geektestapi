
package ResponseForRequestPosts;

import javax.annotation.processing.Generated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "cdnUrl"
})
@Generated("jsonschema2pojo")
public class MainImage {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("cdnUrl")
    private String cdnUrl;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("cdnUrl")
    public String getCdnUrl() {
        return cdnUrl;
    }

    @JsonProperty("cdnUrl")
    public void setCdnUrl(String cdnUrl) {
        this.cdnUrl = cdnUrl;
    }

}
