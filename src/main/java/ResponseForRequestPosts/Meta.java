
package ResponseForRequestPosts;

import javax.annotation.processing.Generated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "prevPage",
    "nextPage",
    "count"
})
@Generated("jsonschema2pojo")
public class Meta {

    @JsonProperty("prevPage")
    private Integer prevPage;
    @JsonProperty("nextPage")
    private Integer nextPage;
    @JsonProperty("count")
    private Integer count;

    @JsonProperty("prevPage")
    public Integer getPrevPage() {
        return prevPage;
    }

    @JsonProperty("prevPage")
    public void setPrevPage(Integer prevPage) {
        this.prevPage = prevPage;
    }

    @JsonProperty("nextPage")
    public Integer getNextPage() {
        return nextPage;
    }

    @JsonProperty("nextPage")
    public void setNextPage(Integer nextPage) {
        this.nextPage = nextPage;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

}
