import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AbstractTestForNotOwnerPosts {

    static Properties prop = new Properties();
    private static InputStream configFile;
    private static String token;
    private static String baseUrl;

    protected static RequestSpecification requestSpecification;
    protected static ResponseSpecification responseSpecification;

    @BeforeAll
    static void initTest() throws IOException {

        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        configFile = new FileInputStream("src/main/resources/my.properties");
        prop.load(configFile);

        token = prop.getProperty("token");
        baseUrl = prop.getProperty("base_url");

        RestAssured.requestSpecification = null;

        requestSpecification = new RequestSpecBuilder()
                .addHeader("X-Auth-Token", token)
                .addQueryParam("owner", "notMe")
                .log(LogDetail.ALL)
                .build();

        RestAssured.requestSpecification = requestSpecification;

    }

    @BeforeEach
    void beforeTest() {

        responseSpecification = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectStatusLine("HTTP/1.1 200 OK")
                .expectContentType(ContentType.JSON)
                .expectResponseTime(Matchers.lessThan(5000L))
                .build();

        RestAssured.responseSpecification = responseSpecification;

    }

    public static String getBaseUrl() {
        return baseUrl;
    }

}
