import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class LoginTest {

    static Properties prop = new Properties();
    private static InputStream configFile;
    private static String baseUrlForLogin;
    private static String myUsername;
    private static String myPassword;
    private static String token;

    @BeforeAll
    static void initTest() throws IOException {

        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        configFile = new FileInputStream("src/main/resources/my.properties");
        prop.load(configFile);

        baseUrlForLogin = prop.getProperty("base_urlForLogin");
        myUsername = prop.getProperty("myUsername");
        myPassword = prop.getProperty("myPassword");
        token = prop.getProperty("token");

        RestAssured.requestSpecification = null;

    }

    @Test
    @Tag("positiveTest")
    void loginWithValidData() {

        JsonPath responseForLogin = given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username",myUsername)
                .formParam("password",myPassword)
                .when()
                .post(baseUrlForLogin).prettyPeek()
                .body()
                .jsonPath();

        assertThat(responseForLogin.get("id"), equalTo(4505));
        assertThat(responseForLogin.get("username"), equalTo(myUsername));
        assertThat(responseForLogin.get("token"), equalTo(token));
    }

    @Test
    @Tag("positiveTest")
    void loginWithInvalidPassword() {

        JsonPath responseForLogin = given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username",myUsername)
                .formParam("password","gb123456")
                .when()
                .post(baseUrlForLogin).prettyPeek()
                .body()
                .jsonPath();

        assertThat(responseForLogin.get("error"), equalTo("Invalid credentials."));
        assertThat(responseForLogin.get("code"), equalTo(401));

    }

    @Test
    @Tag("positiveTest")
    void loginWithInvalidLogin() {

        JsonPath responseForLogin = given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username","Trofimova123")
                .formParam("password",myPassword)
                .when()
                .post(baseUrlForLogin).prettyPeek()
                .body()
                .jsonPath();

        assertThat(responseForLogin.get("error"), equalTo("Invalid credentials."));
        assertThat(responseForLogin.get("code"), equalTo(401));

    }

}
