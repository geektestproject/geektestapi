import ResponseForRequestPosts.ResponseForRequestPosts;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;

public class NotOwnerPostsTest extends AbstractTestForNotOwnerPosts {

    private static int notExistPageForNotOwnerPosts = 100000;

    @Test
    void getPostsWithASCSortNotOwner() {
        ResponseForRequestPosts responseForRequestPosts = given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .when()
                .get(getBaseUrl())
                .then()
                .extract()
                .response()
                .body()
                .as(ResponseForRequestPosts.class);
        assertThat(responseForRequestPosts.getData().isEmpty(), equalTo(false));
        assertThat(responseForRequestPosts.getMeta().getPrevPage(), equalTo(1));
        assertThat(responseForRequestPosts.getMeta().getNextPage(), equalTo(2));

        assertThat(responseForRequestPosts.getData().get(0).getId(), lessThan(responseForRequestPosts.getData().get(1).getId()));
        assertThat(responseForRequestPosts.getData().get(1).getId(), lessThan(responseForRequestPosts.getData().get(2).getId()));
        assertThat(responseForRequestPosts.getData().get(2).getId(), lessThan(responseForRequestPosts.getData().get(3).getId()));

    }

    @Test
    void getPostsWithDESCSortForSecondPageNotOwner() {
        ResponseForRequestPosts responseForRequestPosts = given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", 2)
                .when()
                .get(getBaseUrl())
                .then()
                .extract()
                .response()
                .body()
                .as(ResponseForRequestPosts.class);
        assertThat(responseForRequestPosts.getData().isEmpty(), equalTo(false));
        assertThat(responseForRequestPosts.getMeta().getPrevPage(), equalTo(1));
        assertThat(responseForRequestPosts.getMeta().getNextPage(), equalTo(3));

        assertThat(responseForRequestPosts.getData().get(0).getId(), greaterThan(responseForRequestPosts.getData().get(1).getId()));
        assertThat(responseForRequestPosts.getData().get(1).getId(), greaterThan(responseForRequestPosts.getData().get(2).getId()));
        assertThat(responseForRequestPosts.getData().get(2).getId(), greaterThan(responseForRequestPosts.getData().get(3).getId()));

    }

    @Test
    void getPostsWithoutParamsNotOwner() {
        ResponseForRequestPosts responseForRequestPosts = given()
                .when()
                .get(getBaseUrl())
                .then()
                .extract()
                .response()
                .body()
                .as(ResponseForRequestPosts.class);
        assertThat(responseForRequestPosts.getData().isEmpty(), equalTo(false));
        assertThat(responseForRequestPosts.getMeta().getPrevPage(), equalTo(1));
        assertThat(responseForRequestPosts.getMeta().getNextPage(), equalTo(2));

        assertThat(responseForRequestPosts.getData().get(0).getId(), greaterThan(responseForRequestPosts.getData().get(1).getId()));
        assertThat(responseForRequestPosts.getData().get(1).getId(), greaterThan(responseForRequestPosts.getData().get(2).getId()));
        assertThat(responseForRequestPosts.getData().get(2).getId(), greaterThan(responseForRequestPosts.getData().get(3).getId()));

    }

    @Test
    void getPostsWithASCSortForThirdPageNotOwner() {
        ResponseForRequestPosts responseForRequestPosts = given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "3")
                .when()
                .get(getBaseUrl())
                .then()
                .extract()
                .response()
                .body()
                .as(ResponseForRequestPosts.class);
        assertThat(responseForRequestPosts.getData().isEmpty(), equalTo(false));
        assertThat(responseForRequestPosts.getMeta().getPrevPage(), equalTo(2));
        assertThat(responseForRequestPosts.getMeta().getNextPage(), equalTo(4));

        assertThat(responseForRequestPosts.getData().get(0).getId(), lessThan(responseForRequestPosts.getData().get(1).getId()));
        assertThat(responseForRequestPosts.getData().get(1).getId(), lessThan(responseForRequestPosts.getData().get(2).getId()));
        assertThat(responseForRequestPosts.getData().get(2).getId(), lessThan(responseForRequestPosts.getData().get(3).getId()));

    }

    @Test
    void getPostsForNotExistPageNotOwner() {
        ResponseForRequestPosts responseForRequestPosts = given()
                .queryParam("page", notExistPageForNotOwnerPosts)
                .when()
                .get(getBaseUrl())
                .then()
                .extract()
                .response()
                .body()
                .as(ResponseForRequestPosts.class);
        assertThat(responseForRequestPosts.getData().isEmpty(), equalTo(true));
        assertThat(responseForRequestPosts.getMeta().getPrevPage(), equalTo(notExistPageForNotOwnerPosts - 1));
        assertThat(responseForRequestPosts.getMeta().getNextPage(), equalTo(null));

    }

}
