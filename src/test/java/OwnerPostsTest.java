import ResponseForRequestPosts.ResponseForRequestPosts;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class OwnerPostsTest extends AbstractTestForOwnerPosts {

    private static int myAuthorId = 4505;
    private static int numberOfOwnerPosts = 10;
    private static int notExistPageForOwnerPosts = 20;

    @Test
    void getPostsWithASCSort() {
        ResponseForRequestPosts responseForRequestPosts = given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .when()
                .get(getBaseUrl())
                .then()
                .extract()
                .response()
                .body()
                .as(ResponseForRequestPosts.class);
        assertThat(responseForRequestPosts.getData().isEmpty(), equalTo(false));
        assertThat(responseForRequestPosts.getData().get(1).getAuthorId(), equalTo(myAuthorId));
        assertThat(responseForRequestPosts.getData().get(0).getTitle(), equalTo("Пост 1"));
        assertThat(responseForRequestPosts.getMeta().getCount(), equalTo(numberOfOwnerPosts));
        assertThat(responseForRequestPosts.getMeta().getPrevPage(), equalTo(1));
        assertThat(responseForRequestPosts.getMeta().getNextPage(), equalTo(2));

        assertThat(responseForRequestPosts.getData().get(0).getId(), lessThan(responseForRequestPosts.getData().get(1).getId()));
        assertThat(responseForRequestPosts.getData().get(1).getId(), lessThan(responseForRequestPosts.getData().get(2).getId()));
        assertThat(responseForRequestPosts.getData().get(2).getId(), lessThan(responseForRequestPosts.getData().get(3).getId()));

    }

    @Test
    void getPostsWithDESCSortForSecondPage() {
        ResponseForRequestPosts responseForRequestPosts = given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", 2)
                .when()
                .get(getBaseUrl())
                .then()
                .extract()
                .response()
                .body()
                .as(ResponseForRequestPosts.class);
        assertThat(responseForRequestPosts.getData().isEmpty(), equalTo(false));
        assertThat(responseForRequestPosts.getData().get(0).getAuthorId(), equalTo(myAuthorId));
        assertThat(responseForRequestPosts.getData().get(0).getTitle(), equalTo("Пост 6"));
        assertThat(responseForRequestPosts.getMeta().getCount(), equalTo(numberOfOwnerPosts));
        assertThat(responseForRequestPosts.getMeta().getPrevPage(), equalTo(1));
        assertThat(responseForRequestPosts.getMeta().getNextPage(), equalTo(3));

        assertThat(responseForRequestPosts.getData().get(0).getId(), greaterThan(responseForRequestPosts.getData().get(1).getId()));
        assertThat(responseForRequestPosts.getData().get(1).getId(), greaterThan(responseForRequestPosts.getData().get(2).getId()));
        assertThat(responseForRequestPosts.getData().get(2).getId(), greaterThan(responseForRequestPosts.getData().get(3).getId()));

    }

    @Test
    void getPostsWithoutParams() {
        ResponseForRequestPosts responseForRequestPosts = given()
                .when()
                .get(getBaseUrl())
                .then()
                .extract()
                .response()
                .body()
                .as(ResponseForRequestPosts.class);
        assertThat(responseForRequestPosts.getData().isEmpty(), equalTo(false));
        assertThat(responseForRequestPosts.getData().get(2).getAuthorId(), equalTo(myAuthorId));
        assertThat(responseForRequestPosts.getData().get(0).getTitle(), equalTo("Пост 10"));
        assertThat(responseForRequestPosts.getMeta().getCount(), equalTo(numberOfOwnerPosts));
        assertThat(responseForRequestPosts.getMeta().getPrevPage(), equalTo(1));
        assertThat(responseForRequestPosts.getMeta().getNextPage(), equalTo(2));

        assertThat(responseForRequestPosts.getData().get(0).getId(), greaterThan(responseForRequestPosts.getData().get(1).getId()));
        assertThat(responseForRequestPosts.getData().get(1).getId(), greaterThan(responseForRequestPosts.getData().get(2).getId()));
        assertThat(responseForRequestPosts.getData().get(2).getId(), greaterThan(responseForRequestPosts.getData().get(3).getId()));

    }

    @Test
    void getPostsWithASCSortForThirdPage() {
        ResponseForRequestPosts responseForRequestPosts = given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "3")
                .when()
                .get(getBaseUrl())
                .then()
                .extract()
                .response()
                .body()
                .as(ResponseForRequestPosts.class);
        assertThat(responseForRequestPosts.getData().isEmpty(), equalTo(false));
        assertThat(responseForRequestPosts.getData().get(0).getAuthorId(), equalTo(myAuthorId));
        assertThat(responseForRequestPosts.getData().get(0).getTitle(), equalTo("Пост 9"));
        assertThat(responseForRequestPosts.getMeta().getCount(), equalTo(numberOfOwnerPosts));
        assertThat(responseForRequestPosts.getMeta().getPrevPage(), equalTo(2));
        assertThat(responseForRequestPosts.getMeta().getNextPage(), equalTo(null));

        assertThat(responseForRequestPosts.getData().get(0).getId(), lessThan(responseForRequestPosts.getData().get(1).getId()));

    }

    @Test
    void getPostsForNotExistPage() {
        ResponseForRequestPosts responseForRequestPosts = given()
                .queryParam("page", notExistPageForOwnerPosts)
                .when()
                .get(getBaseUrl())
                .then()
                .extract()
                .response()
                .body()
                .as(ResponseForRequestPosts.class);
        assertThat(responseForRequestPosts.getData().isEmpty(), equalTo(true));
        assertThat(responseForRequestPosts.getMeta().getCount(), equalTo(numberOfOwnerPosts));
        assertThat(responseForRequestPosts.getMeta().getPrevPage(), equalTo(notExistPageForOwnerPosts - 1));
        assertThat(responseForRequestPosts.getMeta().getNextPage(), equalTo(null));

    }

}
